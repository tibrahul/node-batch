var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Enum = require('enum');
const Type = require('../constants/userRoles.js')


var batchdetails_schema = new mongoose.Schema({
    BatchId: {
        type: mongoose.Schema.Types.ObjectId,
        index: true,
        required: true,
        auto: true,
      },
    Batchname:{type:String, default: ''},
    BatchStartDate:{type:Date, default: ''},
    BatchEndDate:{type:Date, default: ''},
    BatchType:{type:String, enum: Type.batchType},
    BatchTime:{type:String, default: ''},
    listofStudents:{ type: String, default: '' },  
    scheduleID:{ type: Schema.Types.ObjectId, ref: 'ScheduleSchool'},
    Instructor:{ type: Schema.Types.ObjectId, ref: 'User'},
});

var batchdetails = mongoose.model('Batch', batchdetails_schema);
module.exports = batchdetails;

