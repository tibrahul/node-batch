var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var reservationdetails_schema = new mongoose.Schema({
    location:{ type: String, default: '' },  
    // date:Date,
    eventid:{type:Schema.Types.ObjectId,ref:"Event"},
    //starttime: { type: Date,default: Date.now},
    //endtime:{ type: Date,default: Date.now},
    starttime:{ type: Date, default: '' },
    endtime:{ type: Date, default: '' },
    participents:{ type: Schema.Types.ObjectId, ref: 'User'},

});

var reservationdetails = mongoose.model('Reservation', reservationdetails_schema);
module.exports = reservationdetails;

