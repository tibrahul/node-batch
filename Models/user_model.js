const bcrypt = require('bcrypt-nodejs');
const crypto = require('crypto');
const mongoose = require('mongoose');
var Enum = require('enum');
const Type = require('../constants/userRoles.js')
var Schema = mongoose.Schema;
var User_schema = new mongoose.Schema({
  name:{ type: String, default: '' },
  dob:{ type: Date, default: '' },
  typeOfUser: {type: String, enum: Type.roleOfUser },
  email: { type: String, unique: true },
  password: { type: String, default:''},
  phone:{ type: Number, default: null },
  address:{ type: String, default: '' },  
  qualification:{ type: String, default: '' },
  dance_id:{ type: Schema.Types.ObjectId, ref: 'dance_category'},
});

User_schema.pre('save', function save(next) {
    const user = this;
    if (!user.isModified('password')) { return next(); }
    bcrypt.genSalt(10, (err, salt) => {
      if (err) { return next(err); }
      bcrypt.hash(user.password, salt, null, (err, hash) => {
        if (err) { return next(err); }
        user.password = hash;
        next();
      });
    });
  });
  
  /**
   * Helper method for validating user's password.
   */
  User_schema.methods.comparePassword = function(candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.password, function(err, isMatch)  {
      if(err) return cb(err)
      cb(err, isMatch);
    });
  };
  
module.exports = mongoose.model('users', User_schema);