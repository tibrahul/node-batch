var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var businessdetails_schema = new mongoose.Schema({
name:{ type: String, default: '' },
location:{ type: String, default: '' },
city:{ type: String, default: '' },
phone:{ type: Number, default: '' },
logo:{ type: Buffer, default: '' },

owner:{ type: Schema.Types.ObjectId, ref: 'User'}
});

var businessdetails = mongoose.model('Business', businessdetails_schema);
module.exports = businessdetails;

