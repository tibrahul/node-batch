var mongoose = require('mongoose');
var user=require('./user_model')
var Schema = mongoose.Schema;


var eventdata = new Schema({
    title:{ type: String, default: '' },
    filledslot:{ type: String, default: '' },
    availableslot:{ type: String, default: '' },
    description:{ type: String, default: '' },
    eventtype:{ type: String, default: '' },
    rate:{ type: String, default:''},
    status:{ type: String, default: '' },
    event_date :{ type: Date, default: '' },
    business_id:{ type: Schema.Types.ObjectId, ref: 'Business'},
    assetid:{ type: Schema.Types.ObjectId, ref: 'Asset'},
    host:{ type: Schema.Types.ObjectId, ref: 'User'},
    dance_id:{ type: Schema.Types.ObjectId, ref: 'dance_category'},
    targetRevenue:{ type: String, default:''},
    totalRevenue:{ type: String, default:''},
    revenueStatus:{ type: String, default:''},

    });
    
    var event = mongoose.model('Event', eventdata);
    module.exports = event;
    