var mongoose = require("mongoose");
var schema = mongoose.Schema;

var payment_details = new schema({
   payment_id :{
    type: mongoose.Schema.Types.ObjectId,
    index: true,
    required: true,
    auto: true,
  
   }, 
   payment_type:{type:String,default:''},
   amount : {type:Number,default:''},
   card_number:{type:Number,default:''}
})

var payment = mongoose.model("payment",payment_details);
module.exports = payment;