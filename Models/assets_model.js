var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var assetsdetails_schema = new mongoose.Schema({
  //  assetid:Number,
    assetname:{ type: String, default: '' },
    assettype:{ type: String, default: '' },//for now it is Non human resources
    assetcategory:{ type: String, default: '' },
    associatedcost:{ type: Number, default: '' },
    businessid:{ type: Schema.Types.ObjectId, ref: 'Business'}
});

var assetdetails = mongoose.model('Asset', assetsdetails_schema);
module.exports = assetdetails;

