var mongoose = require('mongoose');
var AssetSchool=require('./assets_model')
var userSchool=require('./user_model')
var Schema = mongoose.Schema;

var scheduledetails_schema = new mongoose.Schema({
    location:{ type: String, default: '' },  
    date:Date,
    eventid:{type:Schema.Types.ObjectId,ref:"Event"},
    starttime:{ type: String, default: '' },
    endtime:{ type: String, default: '' },
    hosts:{ type: Schema.Types.ObjectId, ref: 'User'},
});

var scheduledetails = mongoose.model('ScheduleSchool', scheduledetails_schema);
module.exports = scheduledetails;

